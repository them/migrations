<?php

declare(strict_types=1);

namespace Them\Migrations\Config;

use Closure;

final class AppConfig
{
    public function __construct(
        public readonly Closure $pdoFactory,
        public readonly SeedConfig $seed,
    ) {
    }

    /**
     * @param mixed $config
     * @return self
     *
     * @throws ConfigurationError
     */
    public static function parse(mixed $config): self
    {
        if ($config instanceof self) {
            return $config;
        }

        return new self(
            ConnectionFactory::create(
                ConfigParser::get($config, 'connection'),
            ),
            SeedConfig::parse(
                ConfigParser::get($config, 'seed'),
            ),
        );
    }
}