<?php

namespace Them\Migrations\Config;

use Closure;
use Generator;
use Stringable;

/**
 * @internal
 */
final class ConfigParser
{
    /**
     * @template T
     *
     * @param mixed $config
     * @param string $key
     *
     * @return null|T
     * @throws ConfigurationError
     */
    public static function get(
        mixed $config,
        string $key,
    ): mixed {
        if (!is_array($config)) {
            throw new ConfigurationError(
                'Invalid configuration format',
            );
        }

        /** @var null|T */
        return $config[$key] ?? null;
    }

    /**
     * @param mixed $config
     * @param string $key
     *
     * @return non-empty-string|null
     * @throws ConfigurationError
     */
    public static function getString(
        mixed $config,
        string $key,
    ): ?string {
        $value = self::get($config, $key);

        if ($value === null) {
            return null;
        }

        if (is_string($value) || $value instanceof Stringable) {
            $value = (string) $value;
        }

        if (is_string($value)) {
            return $value ?: null;
        }

        throw new ConfigurationError(
            "Invalid type for '$key'. Must be string or \\Stringable,",
        );
    }

    /**
     * @param mixed $config
     * @param string $key
     *
     * @return class-string|null
     * @throws ConfigurationError
     */
    public static function getClassString(
        mixed $config,
        string $key,
    ): ?string {
        $class = self::getString($config, $key);
        if ($class === null) {
            return null;
        }

        if (!class_exists($class)) {
            throw new ConfigurationError(
                "Invalid value for '$key'. ' .
                'Class '$class' does not exist",
            );
        }

        return $class;
    }

    /**
     * @param mixed $config
     * @param string $key
     *
     * @return Generator
     * @throws ConfigurationError
     */
    public static function getList(
        mixed $config,
        string $key,
    ): Generator {
        $values = self::get($config, $key);

        if ($values === null) {
            return [];
        }

        if (!is_iterable($values)) {
            throw new ConfigurationError(
                "Invalid type for '$key'. Must be iterable.",
            );
        }

        yield from $values;
    }

    /**
     * @param mixed $config
     * @param string $key
     *
     * @return Generator<string>
     * @throws ConfigurationError
     */
    public static function getStringList(
        mixed $config,
        string $key,
    ): Generator {
        foreach (self::getList($config, $key) as $value) {
            if (!is_string($value)) {
                throw new ConfigurationError(
                    "Invalid type for '$key'. Must be " .
                    "iterable<string> or iterable<\Stringable>",
                );
            }
            yield $value;
        }
    }

    /**
     * @param mixed $config
     * @param string $key
     *
     * @return Generator<trait-string>
     * @throws ConfigurationError
     */
    public static function getTraitList(
        mixed $config,
        string $key,
    ): Generator {
        foreach (self::getStringList($config, $key) as $value) {
            if (!trait_exists($value)) {
                throw new ConfigurationError(
                    "Invalid type for '$key'. " .
                    "Must be iterable<trait-string>",
                );
            }

            yield $value;
        }
    }

    /**
     * @template T
     *
     * @param mixed $config
     * @param string $key
     *
     * @return null|Closure(class-string<T>): T
     * @throws ConfigurationError
     */
    public static function getInitializer(
        mixed $config,
        string $key,
    ): ?Closure {
        $value = self::get($config, $key);

        if ($value === null) {
            return null;
        }

        if (!is_callable($value)) {
            throw new ConfigurationError(
                "Invalid type for '$key'. " .
                "Must be callable(class-string): object",
            );
        }

        /** @var Closure(class-string<T>): T */
        return $value instanceof Closure
            ? $value
            : $value(...);
    }
}
