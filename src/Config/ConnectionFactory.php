<?php

declare(strict_types=1);

namespace Them\Migrations\Config;

use Closure;
use PDO;
use Stringable;

final class ConnectionFactory
{
    /**
     * @param mixed $config
     *
     * @return Closure(): PDO
     * @throws ConfigurationError
     */
    public static function create(mixed $config): Closure
    {
        if ($config instanceof Closure) {
            $factory = $config;
        } elseif (is_callable($config)) {
            $factory = $config(...);
        } elseif (is_string($config) || $config instanceof Stringable) {
            $factory = static fn(): PDO => new PDO((string) $config);
        } elseif ($config instanceof PDO) {
            $factory = static fn(): PDO => $config;
        } else {
            throw new ConfigurationError(
                'Invalid or missing connection configuration',
            );
        }

        /** @var Closure(): PDO */
        return $factory;
    }
}
