<?php

declare(strict_types=1);

namespace Them\Migrations\Config;

use Closure;
use Generator;
use Them\Migrations\SeedInterface;

final class SeedConfig
{
    /**
     * @template T of SeedInterface
     *
     * @param string $path
     * @param string|null $namespace
     * @param class-string|null $extends
     * @param Generator<trait-string> $uses
     * @param null|Closure(class-string<T>): T $initializer
     *
     * @return void
     */
    public function __construct(
        public readonly string $path,
        public readonly ?string $namespace,
        public readonly ?string $extends,
        public readonly Generator $uses,
        public readonly ?Closure $initializer,
    ) {
    }

    /**
     * @param mixed $config
     *
     * @return self
     * @throws ConfigurationError
     */
    public static function parse(mixed $config): self
    {
        if ($config instanceof self) {
            return $config;
        }

        return new self(
            ConfigParser::getString($config, 'path')
                ?? throw new ConfigurationError(
                    'Missing or invalid path for seed files',
                ),
            ConfigParser::getString($config, 'namespace'),
            ConfigParser::getClassString($config, 'extends'),
            ConfigParser::getTraitList($config, 'uses'),
            ConfigParser::getInitializer($config, 'initializer'),
        );
    }
}
