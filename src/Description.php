<?php

namespace Them\Migrations;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
final class Description
{
    public function __construct(
        public readonly string $value,
    ) {
    }
}