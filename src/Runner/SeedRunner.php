<?php

namespace Them\Migrations\Runner;

use Closure;
use Generator;
use PDO;
use RuntimeException;
use Them\Migrations\Config\SeedConfig;
use Them\Migrations\SeedInterface;
use Them\Migrations\Storage\SeedInfo;
use Throwable;

/**
 * @internal
 */
final class SeedRunner
{
    /**
     * @param class-string<SeedInterface> $class
     *
     * @return SeedInterface
     * @throws Throwable
     */
    private static function initialize(string $class): SeedInterface
    {
        return new $class();
    }

    /**
     * @param PDO $pdo
     * @param SeedConfig $config
     * @param SeedInfo ...$infos
     *
     * @return Generator<SeedInfo>
     * @throws Throwable
     */
    public function run(
        PDO        $pdo,
        SeedConfig $config,
        SeedInfo   ...$infos,
    ): Generator {
        $initializer = $config->initializer
            ?? self::initialize(...);

        $pdo->beginTransaction();

        try {
            foreach ($infos as $info) {
                yield $info;

                self::runSeed(
                    $pdo,
                    $initializer,
                    $info->className,
                );
            }

            $pdo->commit();
        } catch (Throwable $exception) {
            $pdo->rollBack();
            throw $exception;
        }
    }

    /**
     * @param PDO $pdo
     * @param Closure $initializer
     * @param class-string<SeedInterface> $className
     *
     * @return void
     */
    private static function runSeed(
        PDO     $pdo,
        Closure $initializer,
        string  $className,
    ): void {
        $seed = $initializer($className);

        if (!$seed instanceof SeedInterface) {
            throw new RuntimeException(
                'Seed initializer returns not a SeedInterface',
            );
        }

        $seed->run($pdo);
    }
}
