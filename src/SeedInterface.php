<?php

declare(strict_types=1);

namespace Them\Migrations;

use PDO;

interface SeedInterface
{
    public function run(PDO $pdo): void;
}