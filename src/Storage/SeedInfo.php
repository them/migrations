<?php

declare(strict_types=1);

namespace Them\Migrations\Storage;

use ReflectionClass;
use ReflectionException;
use Them\Attributes\Attributes;
use Them\Migrations\Config\SeedConfig;
use Them\Migrations\Description;
use Them\Migrations\SeedInterface;

/**
 * @internal
 * @template T of SeedInterface
 */
final class SeedInfo
{
    /**
     * @param string $name
     * @param class-string<T> $className
     * @param string|null $description
     */
    public function __construct(
        public readonly string $name,
        public readonly string $className,
        public readonly ?string $description,
    ) {
    }

    /**
     * @param ReflectionClass<SeedInterface> $reflection
     *
     * @return self
     */
    public static function fromReflection(ReflectionClass $reflection): self
    {
        return new self(
            $reflection->getShortName(),
            $reflection->getName(),
            Attributes::oneFromReflection(
                $reflection,
                Description::class,
            )?->value,
        );
    }

    public static function fromName(
        SeedConfig $config,
        string $name,
    ): ?self {
        /** @var class-string<SeedInterface> $className */
        $className = "$config->namespace\\$name";

        try {
            $reflectionClass = new ReflectionClass($className);
        } catch (ReflectionException) {
            return null;
        }

        return self::fromReflection($reflectionClass);
    }
}
