<?php

declare(strict_types=1);

namespace Them\Migrations\Storage;

use Generator;
use IteratorAggregate;
use ReflectionException;
use RuntimeException;
use Them\Migrations\Config\SeedConfig;
use Them\Migrations\SeedInterface;
use Them\Migrations\Tools\ClassHelper;

/**
 * @internal
 * @implements IteratorAggregate<SeedInfo>
 */
final class SeedRepository implements IteratorAggregate
{
    public function __construct(
        private readonly SeedConfig  $config,
        private readonly ClassHelper $classHelper,
    ) {
        $this->classHelper->includeFiles($this->config->path);
    }

    public function get(string $name): ?SeedInfo {
        return SeedInfo::fromName($this->config, $name);
    }

    /**
     * @param non-empty-string $name
     * @param non-empty-string|null $description
     *
     * @return SeedInfo
     */
    public function create(
        string $name,
        ?string $description = null,
    ): SeedInfo {
        $fileName = sprintf(
            '%s%s%s.php',
            $this->config->path,
            DIRECTORY_SEPARATOR,
            $name,
        );

        if (file_exists($fileName)) {
            throw new RuntimeException(
                "File '$fileName' already exists",
            );
        }

        $body = $this->classHelper->buildClass(
            $name,
            SeedInterface::class,
            ['run'],
            $description,
            $this->config->namespace,
            $this->config->extends,
            $this->config->uses,
        );

        file_put_contents($fileName, (string) $body);

        require $fileName;

        /** @var SeedInfo */
        return SeedInfo::fromName($this->config, $name);
    }

    /**
     * @return Generator<SeedInfo>
     *
     * @throws ReflectionException
     */
    public function getIterator(): Generator
    {
        $implementations = $this->classHelper->findImplementations(
            SeedInterface::class,
        );

        foreach ($implementations as $reflectionClass) {
            yield SeedInfo::fromReflection($reflectionClass);
        }
    }
}
