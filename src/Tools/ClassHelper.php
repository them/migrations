<?php

declare(strict_types=1);

namespace Them\Migrations\Tools;

use Cocur\Slugify\Slugify;
use Generator;
use Nette\PhpGenerator\PhpFile;
use PDO;
use ReflectionClass;
use ReflectionException;
use Stringable;
use Them\Migrations\Description;
use Them\Migrations\SeedInterface;

/**
 * @internal
 */
class ClassHelper
{
    private readonly Slugify $slugifier;

    public function __construct()
    {
        $this->slugifier = Slugify::create([
            "lowercase" => false,
            "separator" => "",
        ]);
    }

    /**
     * @param non-empty-string $name
     * @param class-string $implements
     * @param list<non-empty-string> $methods
     * @param string|null $description
     * @param string|null $namespace
     * @param class-string|null $extends
     * @param iterable<trait-string> $uses
     *
     * @return Stringable
     */
    public function buildClass(
        string $name,
        string $implements,
        array $methods,
        ?string $description = null,
        ?string $namespace = null,
        ?string $extends = null,
        iterable $uses = [],
    ): Stringable {
        $file = new PhpFile();
        $file->setStrictTypes();

        $container = $namespace
            ? $file->addNamespace($namespace)
            : $file;

        $container
            ->addUse(SeedInterface::class)
            ->addUse(PDO::class)
            ->addUse($implements);

        $class = $container
            ->addClass($name)
            ->setImplements([$implements])
            ->setFinal();

        if ($extends) {
            $container->addUse($extends);
            $class->setExtends($extends);
        }

        foreach ($uses as $use) {
            $container->addUse($use);
            $class->addTrait($use);
        }

        if ($description) {
            $container->addUse(Description::class);
            $class->addAttribute(
                Description::class,
                [$description],
            );
        }

        foreach ($methods as $method) {
            $class->addMethod($method)
                ->addBody('// @todo Implement method')
                ->setReturnType('void')
                ->addParameter('pdo')
                ->setType(PDO::class);
        }

        return $file;
    }

    /**
     * @template T
     *
     * @param class-string<T> $interface
     * @return Generator<ReflectionClass<T>>
     *
     * @throws ReflectionException
     */
    public function findImplementations(string $interface): Generator
    {
        foreach (get_declared_classes() as $class) {
            $reflection = new ReflectionClass($class);
            if ($reflection->implementsInterface($interface)) {
                yield $reflection;
            }
        }
    }

    /** @psalm-suppress UnresolvableInclude */
    public function includeFiles(string $path): void
    {
        foreach (glob("$path/*.php") as $fileName) {
            include_once $fileName;
        }
    }

    /**
     * @template T
     *
     * @param class-string<T> $className
     *
     * @return T
     * @psalm-suppress MixedMethodCall
     */
    public static function initialize(string $className): object
    {
        assert(class_exists($className));

        return new $className();
    }

    /**
     * @param non-empty-string $text
     * @return non-empty-string
     */
    function descriptionToSlug(string $text): string
    {
        /** @var non-empty-string */
        return ucfirst(
            $this->slugifier->slugify(
                ucwords($text),
            ),
        );
    }
}
