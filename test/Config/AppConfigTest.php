<?php

declare(strict_types=1);

namespace Them\Migrations\Test\Config;

use Them\Migrations\Config\AppConfig;
use PHPUnit\Framework\TestCase;

final class AppConfigTest extends TestCase
{
    public function testParse(): void
    {
        $subject = AppConfig::parse([
            'connection' => 'sqlite::memory:',
            'seed' => ['path' => 'value'],
        ]);

        $this->assertSame(
            $subject,
            AppConfig::parse($subject),
        );
    }
}
