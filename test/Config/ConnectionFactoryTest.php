<?php

declare(strict_types=1);

namespace Them\Migrations\Test\Config;

use PDO;
use Them\Migrations\Config\ConfigurationError;
use Them\Migrations\Config\ConnectionFactory;
use PHPUnit\Framework\TestCase;

class ConnectionFactoryTest extends TestCase
{
    public function testMissingValue(): void
    {
        $this->expectException(ConfigurationError::class);
        ConnectionFactory::create(null);
    }

    public function testWrongType(): void
    {
        $this->expectException(ConfigurationError::class);
        ConnectionFactory::create([]);
    }

    public function testDsnString(): void
    {
        $configValue = 'sqlite::memory:';

        $this->assertInstanceOf(
            PDO::class,
            (ConnectionFactory::create($configValue))(),
        );
    }

    public function testDsnStringable(): void
    {
        $configValue = new class() {
            public function __toString(): string
            {
                return 'sqlite::memory:';
            }
        };

        $this->assertInstanceOf(
            PDO::class,
            (ConnectionFactory::create($configValue))(),
        );
    }

    public function testPdoInstance(): void
    {
        $configValue = new PDO('sqlite::memory:');

        $this->assertSame(
            $configValue,
            (ConnectionFactory::create($configValue))(),
        );
    }

    public function testPdoClosureFactory(): void
    {
        $pdo = new PDO('sqlite::memory:');
        $configValue = fn(): PDO => $pdo;

        $this->assertSame(
            $pdo,
            (ConnectionFactory::create($configValue))(),
        );
    }

    public function testPdoCallableFactory(): void
    {
        $pdo = new PDO('sqlite::memory:');
        $configValue = new class($pdo) {
            public function __construct(
                private readonly PDO $pdo,
            ) {
            }

            public function __invoke(): PDO
            {
                return $this->pdo;
            }
        };

        $this->assertSame(
            $pdo,
            (ConnectionFactory::create($configValue))(),
        );
    }
}
