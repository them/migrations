<?php

namespace Them\Migrations\Test\Config;

use stdClass;
use Them\Migrations\Config\ConfigurationError;
use Them\Migrations\Config\SeedConfig;
use PHPUnit\Framework\TestCase;
use Them\Migrations\Test\Assets\TestTrait;

final class SeedConfigTest extends TestCase
{
    public function testMissingPath(): void
    {
        $this->expectException(ConfigurationError::class);
        SeedConfig::parse([]);
    }

    public function testParseMinimum(): void
    {
        $config = SeedConfig::parse([
            'path' => 'value',
        ]);

        $this->assertSame(
            'value',
            $config->path,
        );

        $this->assertSame(
            null,
            $config->namespace,
        );

        $this->assertSame(
            null,
            $config->extends,
        );

        $this->assertSame(
            [],
            iterator_to_array($config->uses),
        );

        $this->assertNull($config->initializer);
    }

    public function testParseMaximum(): void
    {
        $config = SeedConfig::parse([
            'path' => 'value',
            'namespace' => 'name\space',
            'extends' => self::class,
            'uses' => [TestTrait::class],
            'initializer' => fn(string $class) => new $class(),
        ]);

        $this->assertSame(
            'value',
            $config->path,
        );

        $this->assertSame(
            'name\space',
            $config->namespace,
        );

        $this->assertSame(
            self::class,
            $config->extends,
        );

        $this->assertSame(
            [TestTrait::class],
            iterator_to_array($config->uses),
        );

        $this->assertNotNull($config->initializer);

        $this->assertInstanceOf(
            stdClass::class,
            ($config->initializer)(stdClass::class)
        );
    }

    public function testParseSelf(): void
    {
        $config = new SeedConfig(
            'value',
            null,
            null,
            (fn() => yield)(),
            null,
        );

        $this->assertSame(
            $config,
            SeedConfig::parse($config),
        );
    }
}
